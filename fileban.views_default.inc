<?php

/**
 * @file
 * Contains default views on behalf of the fileban module.
 */

/**
 * Implementation of hook_views_default_views().
 */
function fileban_views_default_views() {
  // The following lines aren't indented but should be...
$view = new view;
$view->name = 'file_manager';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'file_managed';
$view->human_name = 'File manager';
$view->core = 7;
$view->api_version = '3.0-alpha1';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'File manager';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'administer file ban';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '20';
$handler->display->display_options['style_plugin'] = 'bulk';
/* Field: File: File ID */
$handler->display->display_options['fields']['fid']['id'] = 'fid';
$handler->display->display_options['fields']['fid']['table'] = 'file_managed';
$handler->display->display_options['fields']['fid']['field'] = 'fid';
$handler->display->display_options['fields']['fid']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['fid']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['fid']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['fid']['alter']['external'] = 0;
$handler->display->display_options['fields']['fid']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['fid']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['fid']['alter']['trim'] = 0;
$handler->display->display_options['fields']['fid']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['fid']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['fid']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['fid']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['fid']['alter']['html'] = 0;
$handler->display->display_options['fields']['fid']['element_label_colon'] = 1;
$handler->display->display_options['fields']['fid']['element_default_classes'] = 1;
$handler->display->display_options['fields']['fid']['hide_empty'] = 0;
$handler->display->display_options['fields']['fid']['empty_zero'] = 0;
$handler->display->display_options['fields']['fid']['link_to_file'] = 1;
/* Field: File: Name */
$handler->display->display_options['fields']['filename']['id'] = 'filename';
$handler->display->display_options['fields']['filename']['table'] = 'file_managed';
$handler->display->display_options['fields']['filename']['field'] = 'filename';
$handler->display->display_options['fields']['filename']['label'] = 'File name';
$handler->display->display_options['fields']['filename']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['filename']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['filename']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['filename']['alter']['external'] = 0;
$handler->display->display_options['fields']['filename']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['filename']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['filename']['alter']['trim'] = 0;
$handler->display->display_options['fields']['filename']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['filename']['alter']['word_boundary'] = 0;
$handler->display->display_options['fields']['filename']['alter']['ellipsis'] = 0;
$handler->display->display_options['fields']['filename']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['filename']['alter']['html'] = 0;
$handler->display->display_options['fields']['filename']['element_label_colon'] = 1;
$handler->display->display_options['fields']['filename']['element_default_classes'] = 1;
$handler->display->display_options['fields']['filename']['hide_empty'] = 0;
$handler->display->display_options['fields']['filename']['empty_zero'] = 0;
$handler->display->display_options['fields']['filename']['link_to_file'] = 1;
/* Field: File: Mime type */
$handler->display->display_options['fields']['filemime']['id'] = 'filemime';
$handler->display->display_options['fields']['filemime']['table'] = 'file_managed';
$handler->display->display_options['fields']['filemime']['field'] = 'filemime';
$handler->display->display_options['fields']['filemime']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['filemime']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['filemime']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['filemime']['alter']['external'] = 0;
$handler->display->display_options['fields']['filemime']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['filemime']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['filemime']['alter']['trim'] = 0;
$handler->display->display_options['fields']['filemime']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['filemime']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['filemime']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['filemime']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['filemime']['alter']['html'] = 0;
$handler->display->display_options['fields']['filemime']['element_label_colon'] = 1;
$handler->display->display_options['fields']['filemime']['element_default_classes'] = 1;
$handler->display->display_options['fields']['filemime']['hide_empty'] = 0;
$handler->display->display_options['fields']['filemime']['empty_zero'] = 0;
$handler->display->display_options['fields']['filemime']['link_to_file'] = 0;
$handler->display->display_options['fields']['filemime']['filemime_image'] = 0;
/* Field: File: Size */
$handler->display->display_options['fields']['filesize']['id'] = 'filesize';
$handler->display->display_options['fields']['filesize']['table'] = 'file_managed';
$handler->display->display_options['fields']['filesize']['field'] = 'filesize';
$handler->display->display_options['fields']['filesize']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['filesize']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['filesize']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['filesize']['alter']['external'] = 0;
$handler->display->display_options['fields']['filesize']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['filesize']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['filesize']['alter']['trim'] = 0;
$handler->display->display_options['fields']['filesize']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['filesize']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['filesize']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['filesize']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['filesize']['alter']['html'] = 0;
$handler->display->display_options['fields']['filesize']['element_label_colon'] = 1;
$handler->display->display_options['fields']['filesize']['element_default_classes'] = 1;
$handler->display->display_options['fields']['filesize']['hide_empty'] = 0;
$handler->display->display_options['fields']['filesize']['empty_zero'] = 0;
/* Field: File Usage: Module */
$handler->display->display_options['fields']['module']['id'] = 'module';
$handler->display->display_options['fields']['module']['table'] = 'file_usage';
$handler->display->display_options['fields']['module']['field'] = 'module';
$handler->display->display_options['fields']['module']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['module']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['module']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['module']['alter']['external'] = 0;
$handler->display->display_options['fields']['module']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['module']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['module']['alter']['trim'] = 0;
$handler->display->display_options['fields']['module']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['module']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['module']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['module']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['module']['alter']['html'] = 0;
$handler->display->display_options['fields']['module']['element_label_colon'] = 1;
$handler->display->display_options['fields']['module']['element_default_classes'] = 1;
$handler->display->display_options['fields']['module']['hide_empty'] = 0;
$handler->display->display_options['fields']['module']['empty_zero'] = 0;
/* Field: File: Upload date */
$handler->display->display_options['fields']['timestamp']['id'] = 'timestamp';
$handler->display->display_options['fields']['timestamp']['table'] = 'file_managed';
$handler->display->display_options['fields']['timestamp']['field'] = 'timestamp';
$handler->display->display_options['fields']['timestamp']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['timestamp']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['timestamp']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['timestamp']['alter']['external'] = 0;
$handler->display->display_options['fields']['timestamp']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['timestamp']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['timestamp']['alter']['trim'] = 0;
$handler->display->display_options['fields']['timestamp']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['timestamp']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['timestamp']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['timestamp']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['timestamp']['alter']['html'] = 0;
$handler->display->display_options['fields']['timestamp']['element_label_colon'] = 1;
$handler->display->display_options['fields']['timestamp']['element_default_classes'] = 1;
$handler->display->display_options['fields']['timestamp']['hide_empty'] = 0;
$handler->display->display_options['fields']['timestamp']['empty_zero'] = 0;
$handler->display->display_options['fields']['timestamp']['date_format'] = 'short';
/* Sort criterion: File: Upload date */
$handler->display->display_options['sorts']['timestamp']['id'] = 'timestamp';
$handler->display->display_options['sorts']['timestamp']['table'] = 'file_managed';
$handler->display->display_options['sorts']['timestamp']['field'] = 'timestamp';
$handler->display->display_options['sorts']['timestamp']['order'] = 'DESC';

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'bulk';
$handler->display->display_options['style_options']['columns'] = array(
  'filename' => 'filename',
  'fid' => 'fid',
  'filemime' => 'filemime',
  'filesize' => 'filesize',
  'timestamp' => 'timestamp',
  'module' => 'module',
);
$handler->display->display_options['style_options']['default'] = 'timestamp';
$handler->display->display_options['style_options']['info'] = array(
  'filename' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'fid' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'filemime' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'filesize' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'timestamp' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'module' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
);
$handler->display->display_options['style_options']['override'] = 1;
$handler->display->display_options['style_options']['sticky'] = 0;
$handler->display->display_options['style_options']['order'] = 'desc';
$handler->display->display_options['style_options']['empty_table'] = 0;
$handler->display->display_options['style_options']['selected_operations'] = array(
  'fileban_ban_action' => 'fileban_ban_action',
  'fileban_unban_action' => 'fileban_unban_action',
  'system_message_action' => 0,
  'views_bulk_operations_action' => 0,
  'views_bulk_operations_script_action' => 0,
  'views_bulk_operations_argument_selector_action' => 0,
  'system_goto_action' => 0,
  'system_send_email_action' => 0,
);
$handler->display->display_options['style_options']['execution_type'] = '1';
$handler->display->display_options['style_options']['display_type'] = '0';
$handler->display->display_options['style_options']['skip_confirmation'] = 0;
$handler->display->display_options['style_options']['display_result'] = 1;
$handler->display->display_options['style_options']['merge_single_action'] = 1;
$handler->display->display_options['style_options']['hide_select_all'] = 0;
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['path'] = 'admin/content/file';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'File manager';
$handler->display->display_options['menu']['weight'] = '0';
$translatables['file_manager'] = array(
  t('Master'),
  t('File manager'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('File ID'),
  t('File name'),
  t('Mime type'),
  t('Size'),
  t('Module'),
  t('Upload date'),
  t('Page'),
);
  $views[$view->name] = $view;
  return $views;
}
