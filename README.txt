FILE BAN
--------

File ban module allows site administrators to ban (and unban) files from 
being uploaded to the site. Files are identified by their SHA-256 hash.

When a file is banned, it is copied to a new file object in the private 
file system for archival purposes.

Views Bulk Operations module is required to provide the file management 
user interface.
